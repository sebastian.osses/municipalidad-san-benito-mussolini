import {DataTypes} from 'sequelize';
import {sequelize} from '../database/database.js';

export const Infraction = sequelize.define('infractions', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement:true
    },
    inspectorId: {
        type: DataTypes.INTEGER
    },
    patente: {
        type: DataTypes.STRING
    },
    rut: {
        type: DataTypes.STRING
    },
    tipo: {
        type: DataTypes.STRING
    },
    criticidad: {
        type: DataTypes.STRING
    },
    fecha_emision: {
        type: DataTypes.DATE
    },
    fecha_vencimiento: {
        type: DataTypes.DATE
    },
    detalles: {
        type: DataTypes.STRING(512),
        allowNull: true
    },
    evidencia: {
        type: DataTypes.BLOB('long'),
        allowNull: true
    },
    monto: {
        type: DataTypes.INTEGER
    },
    estado: {
        type: DataTypes.STRING,
        defaultValue: 'activa'
    }
}, {
    timestamps: false, // Deshabilitar createdAt y updatedAt
  hooks: {
    beforeSave: (infraction, options) => {
      if (infraction.fecha_vencimiento && new Date() >= infraction.fecha_vencimiento) {
        infraction.estado = 'vencida';
      }
    }
  }
});