import { DataTypes } from 'sequelize';
import { sequelize } from '../database/database.js';
import { Person } from './Person.js';

export const Patent = sequelize.define('patents', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  rut_person: {
    type: DataTypes.STRING,
  },
  patente: {
    type: DataTypes.STRING,
    unique: true
    }
}, {
  timestamps: false, // Deshabilitar createdAt y updatedAt
});

// Definir la relación 1:a muchos con la tabla Person
Person.hasMany(Patent, {
    foreignKey: 'rut_person',
    sourceKey: 'rut'
  });
  
Patent.belongsTo(Person, {
    foreignKey: 'rut_person',
    targetId: 'rut',
    allowNull: false, // Makes the association required
});