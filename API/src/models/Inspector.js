import {DataTypes} from 'sequelize';
import {sequelize} from '../database/database.js';
import {Infraction} from './Infraction.js';

export const Inspector = sequelize.define('inspectors', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement:true
    },
    nombre: {
        type: DataTypes.STRING
    },
    apellido: {
        type: DataTypes.STRING
    },
    email: {
        type: DataTypes.STRING
    },
    password: {
        type: DataTypes.STRING
    },tipo:{
        type: DataTypes.STRING,
        defaultValue: 'inspector'
    }
}, {
    timestamps: false, // Deshabilitar createdAt y updatedAt
});

// relacion entre inspector e infraccion: (inspector tiene muchas infracciones)
Inspector.hasMany(Infraction, {
    foreignKey: 'inspectorId',
    sourceKey: 'id'
})

Infraction.belongsTo(Inspector, {
    foreignKey: 'inspectorId',
    targetId: 'id'
})
