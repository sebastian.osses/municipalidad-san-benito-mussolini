import {DataTypes} from 'sequelize';
import {sequelize} from '../database/database.js';
import {Infraction} from './Infraction.js';

export const Person = sequelize.define('persons', {
    id:{
        type:DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement: true
    },
    rut: {
        type: DataTypes.STRING,
        unique: true,
    },
    nombre: {
        type: DataTypes.STRING
    },
    apellido: {
        type: DataTypes.STRING
    },
    fecha_nacimiento: {
        type: DataTypes.DATE
    },
    domicilio: {
        type: DataTypes.STRING
    },
    telefono: {
        type: DataTypes.STRING
    },
    email: {
        type: DataTypes.STRING
    }
    //patente: {
     //   type: DataTypes.STRING,
     //   unique: true
    //}
}, {
    timestamps: false, // Deshabilitar createdAt y updatedAt
  });
// relacion persona e infraccion: (una persona tiene muchas infracciones)

Person.hasMany(Infraction, {
    foreignKey: 'rut',
    sourceKey: 'rut'
})

Infraction.belongsTo(Person, {
    foreignKey: 'rut',
    targetId: 'rut'
})
