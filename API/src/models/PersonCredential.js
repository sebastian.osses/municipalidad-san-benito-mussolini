import { DataTypes } from 'sequelize';
import { sequelize } from '../database/database.js';
import { Person } from './Person.js';

export const PersonCredential = sequelize.define('personCredentials', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  rut_person: {
    type: DataTypes.STRING,
    unique: true,
  },
  email: {
    type: DataTypes.STRING,
  },
  password: {
    type: DataTypes.STRING,
  },
}, {
  timestamps: false, // Deshabilitar createdAt y updatedAt
});

// Definir la relación 1:1 con la tabla Person
Person.hasOne(PersonCredential, {
    foreignKey: 'rut_person',
    sourceKey: 'rut'
  });
  
PersonCredential.belongsTo(Person, {
    foreignKey: 'rut_person',
    targetId: 'rut',
    allowNull: false, // Makes the association required
});