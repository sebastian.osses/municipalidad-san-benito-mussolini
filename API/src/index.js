import app from './app.js';
import { sequelize } from './database/database.js';
import { Person } from './models/Person.js';
import { Patent } from './models/Patent.js';
import { verificarPatente, verificarRut } from '../functions/validation_fuctions.js';


import data from '../Persons.json' assert { type: "json" };
import moment from 'moment'; 

const port = 3000;

async function main() {
  try {
    await sequelize.sync({ force: false});

    // Insertar personas en la base de datos
    await insertPersons();

    app.listen(port);
    console.log('Server is listening on port', port);
  } catch (error) {
    console.error("Unable to connect to the database");
  }
}

async function insertPersons() {
  try {
    const personsData = data.persons;

    for (const person of personsData) {
      const uppercaseRut = person.rut.toUpperCase();

      if (!verificarRut(uppercaseRut)) {
        console.log(`RUT inválido: ${person.rut}`);
        continue; // Salta al siguiente si el RUT no es válido
      }

      // Formatear la fecha
      const formattedPerson = {
        ...person,
        rut: uppercaseRut, // Asigna el RUT en mayúsculas
        fecha_nacimiento: moment(person.fecha_nacimiento, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss[Z]'),
      };

      // Buscar la persona en la base de datos por su RUT
      const existingPerson = await Person.findOne({ where: { rut: formattedPerson.rut } });

      if (existingPerson) {
        // La persona ya existe, actualiza los campos modificados
        await existingPerson.update(formattedPerson);
        console.log(`Persona con RUT ${formattedPerson.rut} actualizada correctamente.`);
      } else {
        // La persona no existe, crea una nueva entrada
        await Person.create(formattedPerson);
        console.log(`Persona con RUT ${formattedPerson.rut} insertada correctamente.`);
      }

      // Obtener la relación de patentes de la persona
      const patents = person.patentes;

      for (const patent of patents) {
        const uppercasePatent = patent.toUpperCase();

        if (!verificarPatente(uppercasePatent)) {
          console.log(`Patente inválida: ${patent}`);
          continue; // Salta al siguiente si la patente no es válida
        }

        const [newPatent, createdPatent] = await Patent.findOrCreate({
          where: {
            rut_person: formattedPerson.rut,
            patente: uppercasePatent, // Asigna la patente en mayúsculas
          },
        });

        if (createdPatent) {
          console.log(`Patente ${uppercasePatent} asociada a la persona con RUT ${formattedPerson.rut} insertada correctamente.`);
        } else {
          console.log(`La patente ${uppercasePatent} ya existe en la base de datos para la persona con RUT ${formattedPerson.rut}. No se realizó ninguna inserción.`);
        }
      }
    }

    console.log('Proceso de inserción/actualización finalizado.');
  } catch (error) {
    console.error('Error al insertar/actualizar personas:', error);
  }
}


main();