import {Router} from 'express';
import { getInfractions, createInfraction, getInfractionId, getInfractionImage, payInfraction, appealInfraction } from '../controllers/infractions.controller.js';
import multer from 'multer';

const router = Router();
// Configura multer
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/'); // Carpeta donde se guardarán los archivos
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname); // Mantener el nombre original del archivo
    },
});

const upload = multer({ storage });

//////


router.get('/infractions', getInfractions);
router.get('/infractions/:id', getInfractionId);

// pago
router.put('/infractions/pay/:id', payInfraction);

//apelacion
router.put('/infractions/appeal/:id', appealInfraction)

router.delete('/infractions/:id');


router.post('/infractions', upload.single('evidencia'), createInfraction);


router.get('/uploads/:fileName', getInfractionImage);

export default router;