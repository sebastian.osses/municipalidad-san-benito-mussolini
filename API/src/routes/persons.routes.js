import {Router} from 'express';
import { getPerson, createPersonCred, createPerson, getPersonInfractions, getPersonById } from '../controllers/persons.controller.js';

const router = Router();

router.get('/person', getPerson);

router.get('/person/:id', getPersonById);

router.get('/person/:id/infractions', getPersonInfractions);

router.post('/person', createPersonCred);
router.post('/godmode', createPerson);


export default router;