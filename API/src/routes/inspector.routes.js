import {Router} from 'express';
import { getInspector, createInspector, getInspectorInfractions } from '../controllers/inspector.controller.js';

const router = Router();

router.get('/inspector', getInspector);

router.get('/inspector/:id/infractions', getInspectorInfractions)

router.post('/inspector', createInspector);


export default router;