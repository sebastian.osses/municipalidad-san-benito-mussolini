
import {Infraction} from "../models/Infraction.js";
import { Inspector } from '../models/Inspector.js'; // Importa el modelo de Inspector
import {Person} from '../models/Person.js'; // Importa el modelo de persona
import { Patent } from "../models/Patent.js";
import { verificarRut, verificarPatente, formatDate } from "../../functions/validation_fuctions.js";

import { fileURLToPath } from 'url';
import { basename, dirname } from 'path';
import path from 'path'; 
import { Op } from 'sequelize';
import { transporter } from '../app.js';

// funciones para los verbos http

//get
export const getInfractions = async (req, res) => {
    try{ 
        const infractionArray = await Infraction.findAll();
    
        return res.status(200).json(infractionArray);
    }catch (error){
        return res.status(500).json({message: error.message})
    } 
}

//get image
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// Resto del código del controlador
export const getInfractionImage = (req, res) => {
    const fileName = req.params.fileName;
    const filePath = path.join(__dirname, '../../uploads', fileName);

    res.sendFile(filePath, (err) => {
        if (err) {
            console.error(err);
            res.status(404).json({ message: 'Imagen no encontrada' });
        }
    });
};

export const getInfractionId = async (req, res) => {
    const { id } = req.params;
    try {
        console.log('ID de la infracción solicitada:', id);

        const infraction = await Infraction.findByPk(id);

        if (!infraction) {
            console.log('No se encontró la infracción');
            return res.status(404).json({ message: 'Infracción no encontrada' });
        }

        return res.status(200).json(infraction);
    } catch (error) {
        console.error('Error en getInfractionId:', error);
        return res.status(500).json({ message: error.message });
    }
};

//post
export const createInfraction = async (req, res) => {
    try {
        const inspectorId = req.body.inspectorId;
        //const rut = req.body.rut.toUpperCase();
        const patente = req.body.patente.toUpperCase();
        const tipo = req.body.tipo;
        const criticidad = req.body.criticidad;
        const detalles = req.body.detalles;
        const monto = req.body.monto;

        const fecha_emision = new Date();

        const fecha_vencimiento = new Date(fecha_emision);
        fecha_vencimiento.setMonth(fecha_emision.getMonth() + 1); // por si se vence en 1 mes


        // Verifica si req.file está definido
        if (!req.file) {
            return res.status(400).json({ message: 'No se ha proporcionado un archivo de evidencia' });
        }
        // Logica de validacion

        if (!verificarPatente(patente)){
            console.log("PATENTE INVALIDA");
            return res.status(401).json({ message: 'Formato de patente invalido'}); 
        }

        if (tipo.trim() == "") {
            console.log("No se especifica tipo de infraccion");

            return res.status(402).json({ message: 'No se especifica tipo de infracción'}); 
        }

        // Buscar el inspector por ID
        const foundInspector = await Inspector.findByPk(inspectorId);

        if (!foundInspector) {
            console.log("INSPECTOR NO ENCONTRADO");
            return res.status(403).json({ message: 'Inspector no encontrado' });
        }


        // Buscar la persona por Patente en la tabla 'patents'
        const foundPatent = await Patent.findOne({
            where: {
            patente: {
                [Op.eq]: patente, // Busca por igualdad de patente
            },
            },
        });
        
        if (!foundPatent) {
            console.log("PATENTE NO ASOCIADA A PERSONA");
            return res.status(405).json({ message: 'Patente no asociada a ninguna persona' });
        }
        
        // Obtener el rut de la persona asociada a la patente
        const rut = foundPatent.rut_person;
        

        // Buscar la persona en la tabla 'persons' utilizando el rut
        const foundPerson = await Person.findOne({
            where: {
            rut: {
                [Op.eq]: rut, // Busca por igualdad de rut
            },
            },
        });
        
        if (!foundPerson) {
            console.log("PERSONA NO ENCONTRADA");
            return res.status(404).json({ message: 'Persona no encontrada' });
        }

        
        // Validar si el monto es menor o igual a cero pesos
        if (monto <= 0) {
            console.error("Error: El monto debe ser mayor a 0 CLP.");
            // Puedes retornar un mensaje de error o manejar la situación de otra manera
            return res.status(406).json({ message: 'El monto debe ser mayor a 0 CLP.' });
        }


        // Creacion de la persona
        const newInfraction = await Infraction.create({
            inspectorId,
            rut,
            patente,
            tipo,
            criticidad,
            fecha_emision,
            fecha_vencimiento,
            detalles,
            monto,
            evidencia: req.file.filename, // Guardar el nombre del archivo en la base de datos
        }
        );

        console.log(newInfraction);

        // Envio correo infraccion xd

        let email_infraccionado = foundPerson.email;

        try {
            const correo_notificacion = await transporter.sendMail({
                from: '"Usted ha recibido una infracción de tránsito 👎" <municipalidad.saint.benedict@gmail.com>',
                to: email_infraccionado,
                subject: "Infracción de Tránsito",
                text: "Usted ha recibido una infracción de tránsito 👎",
                html: `
                    <b>Tipo de Infracción:</b> ${tipo}<br>
                    <b>Detalles:</b> ${detalles}<br>
                    <b>Imagen de Evidencia:</b><br>
                    <img src="cid:unique@nodemailer.com"/><br><br>
                    <b>Para revisar la infracción en mayor detalle, ingrese a: </b> <a href="http://localhost:3001/">Portal de Infracciones Municipalidad San Benito</a><br>`,
                attachments: [{
                    filename: req.file.filename,
                    path: 'uploads/' + req.file.filename,
                    cid: 'unique@nodemailer.com'
                }]
            });
        
            console.log('Correo enviado exitosamente:', correo_notificacion.response);
        } catch (error) {
            console.error('Error al enviar el correo:', error);
        }

        return res.status(201).json({ message: 'Infracción creada exitosamente', newInfraction });
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
};


//PUT
export const payInfraction = async (req, res) => {
    try {
        const id = req.body.id;
        const rut = req.body.rut;
        const bank = req.body.bank;
        console.log(`el id que llego es : ${id}`);

    // Validar el formato del rut
    if (!verificarRut(rut)) {
        return res.status(400).json({ message: 'Formato de rut inválido' });
    }

    // Buscar la infracción por ID
    const infraccion = await Infraction.findByPk(id);

    if (!infraccion) {
        return res.status(404).json({ message: 'Infracción no encontrada' });
    }

    // Verificar que el rut coincida con el de la infracción
    if (infraccion.rut !== rut) {
        return res.status(401).json({ message: 'El rut no coincide con el de la infracción' });
    }

    // Verificar si la infracción ya está pagada
    if (infraccion.estado === 'pagada') {
        return res.status(402).json({ message: 'La infracción ya está pagada' });
    }

    // Actualizar el estado de la infracción a 'Pagada'


    infraccion.estado = 'pagada';
    await infraccion.save();

    // Envio correo pago xd
    const foundPerson = await Person.findOne({
        where: {
        rut: {
            [Op.eq]: rut, // Busca por igualdad de rut
        },
        },
    });
    
    if (!foundPerson) {
        console.log("PERSONA NO ENCONTRADA");
        return res.status(406).json({ message: 'Persona no encontrada' });
    }

    let email_infraccionado = foundPerson.email;
    console.log(email_infraccionado);

    const fecha_pago = new Date();

    try {
        const correo_notificacion = await transporter.sendMail({
            from: '"Usted ha pagado una infracción de tránsito 👍" <municipalidad.saint.benedict@gmail.com>',
            to: email_infraccionado,
            subject: "Comprobante de pago",
            text: `Usted ha pagado una infracción de tránsito 👍`,
            html: `
                <b>Fecha:</b> ${formatDate(fecha_pago)}<br>
                <b>Nombre:</b> ${foundPerson.nombre} ${foundPerson.apellido}<br>
                <b>Banco:</b> ${bank}<br>
                <b>Monto:</b> ${infraccion.monto}<br>
                <b>Id Infracción:</b>${infraccion.id}<br>`
        });
    
        console.log('Correo enviado exitosamente:', correo_notificacion.response);
    } catch (error) {
        console.error('Error al enviar el correo:', error);
    }


    return res.status(200).json({ message: 'Pago verificado correctamente', infraccion });
    } catch (error) {
    console.error('Error en payInfraction:', error);
    return res.status(500).json({ message: 'Error interno del servidor' });
    }
};


// PUT
export const appealInfraction = async (req, res) => {
    try {
        const { id } = req.params;

        // Buscar la infracción por ID
        const infraccion = await Infraction.findByPk(id);

        if (!infraccion) {
            return res.status(404).json({ message: 'Infracción no encontrada' });
        }

        // Verificar si la infracción ya está en apelación
        if (infraccion.estado === 'en apelacion') {
            return res.status(402).json({ message: 'La infracción ya está en apelación' });
        }

        // Actualizar el estado de la infracción a 'En Apelación'
        infraccion.estado = 'en apelacion';
        await infraccion.save();

        return res.status(200).json({ message: 'Apelación registrada correctamente', infraccion });
    } catch (error) {
        console.error('Error en appealInfraction:', error);
        return res.status(500).json({ message: 'Error interno del servidor' });
    }
};