import { Person } from '../models/Person.js'; // Importa el modelo de person
import { PersonCredential } from '../models/PersonCredential.js'; // Importa el modelo de personCred
import { Infraction } from '../models/Infraction.js';
import { Patent } from '../models/Patent.js';
import { Op } from 'sequelize';
import { verificarRut, verificarPatente } from "../../functions/validation_fuctions.js";

// get
export const getPerson = async (req, res) => {
  const { email, password } = req.query; // Obtén el email y la contraseña de la solicitud

  try {
    const personCred = await PersonCredential.findOne({
      where: {
        email: {
          [Op.eq]: email, // Busca por igualdad de email
        },
        password: {
          [Op.eq]: password, // Busca por igualdad de contraseña
        },
      },
    });

    const person = await Person.findOne({
        where: {
            rut: {
              [Op.eq]: personCred.rut_person, // Busca por igualdad de email
            }
        }
    });

    if (person) {
      // Si se encuentra el person con las credenciales
      res.status(200).json(person);
    } else {
      // Si no se encuentra el person
      res.status(401).json({ message: 'Credenciales inválidas' });
    }
  } catch (error) {
    // Si ocurre un error en la consulta
    res.status(500).json({ message: 'Error al buscar persona' });
  }
};

// get person by id
export const getPersonById = async (req, res) => {
  const { id } = req.params; // Obtén el ID de la solicitud

  try {
    // Busca a la persona por su ID
    const person = await Person.findOne({
      where: {
        id: {
          [Op.eq]: id,
        },
      },
    });

    if (!person) {
      // Si no se encuentra a la persona
      return res.status(404).json({ message: 'Persona no encontrada' });
    }

    const patents = await Patent.findAll({
      where: {
        rut_person: {
          [Op.eq]: person.rut,
        },
      },
    });
    console.log(patents);
    // Agrega las patentes a la respuesta
    const responsePerson = {
      ...person.toJSON(),
      patentes: patents ? patents.map(patent => patent.patente) : [],
    };
    
    // Devuelve la persona con las patentes en la respuesta
    return res.status(200).json(responsePerson);
  } catch (error) {
    // Si ocurre un error en la consulta
    console.error('Error al obtener la persona por ID:', error);
    return res.status(500).json({ message: 'Error al obtener la persona por ID' });
  }
};


//get infracciones de la persona // POR RUT
export const getPersonInfractions = async (req, res) => {
  const { id } = req.params;
  console.log("id obtenido", id);

  try {
    // Buscar al persona por ID
    const person = await Person.findOne({
      where: {
        id: {
          [Op.eq]: id,
        },
      },
    });

    if (!person) {
      // Si no se encuentra persona
      return res.status(404).json({ message: 'Persona no encontrada' });
    }

    // Buscar las infracciones del inspector por su ID
    const infractions = await Infraction.findAll({
      where: {
        rut: {
          [Op.eq]: person.rut,
        },
      },
    });

    // Transforma el resultado para obtener el nombre del archivo
    const transformedInfractions = infractions.map(infraction => {
      const fileName = infraction.evidencia ? infraction.evidencia.toString().split('/').pop() : null;
      console.log(`el nombre del archivo es: ${fileName}`);
      return {
        ...infraction.dataValues,
        evidencia: fileName,
      };
    });

    // Devolver las infracciones con el nombre del archivo
    console.log(transformedInfractions);
    return res.status(200).json(transformedInfractions);
  } catch (error) {
    // Si ocurre un error en la consulta
    console.error('Error al obtener las infracciones de la persona:', error);
    return res.status(500).json({ message: 'Error al obtener las infracciones de la persona' });
  }
};

//post
export const createPersonCred = async (req, res) => {
    const { rut, email, password } = req.body; // Obtén los datos del inspector desde el cuerpo de la solicitud

    try {
        // Verifica si ya existe un inspector con el mismo correo electrónico
        const existingCred = await PersonCredential.findOne({
            where: {
                email: {
                    [Op.eq]: email, // Busca por igualdad de email
                },
            },
        });

        const registeredRut = await PersonCredential.findOne({
            where: {
                rut_person: {
                    [Op.eq]: rut, // Busca por igualdad de rut
                },
            },
        });

        // Verifica si ya existe una persona con el mismo RUT
        const existingPerson = await Person.findOne({
            where: {
                rut: {
                    [Op.eq]: rut, // Busca por igualdad de RUT
                },
            },
        });

        if (existingCred) {
            // Si ya existe un inspector con el mismo correo electrónico
            return res.status(400).json({ message: 'El correo electrónico ya está registrado' });
        }

        if (registeredRut) {
            // Si ya existe una cuenta con el mismo RUT
            return res.status(401).json({ message: 'Ya existe una cuenta para ese RUT' });
        }
        
        if (!existingPerson) {
            // La persona no existe
            return res.status(402).json({ message: 'RUT inválido' });
        } else {
            // Crea un nuevo inspector en la base de datos
            const rut_person = rut;
            const newPersonCredential = await PersonCredential.create({
                rut_person,
                email,
                password,
            });

            // Envía una respuesta con el nuevo inspector creado
            //console.log(newPersonCredential);
            res.status(201).json({ message: 'Usuario creado correctamente.' });
        }
    } catch (error) {
        // Si ocurre un error en la creación
        console.error('Error en la solicitud:', error);
        res.status(500).json({ message: 'Error al registrar la cuenta' });
    }
};


//crear ciudadano (modo dios)
export const createPerson = async (req, res) => {
  let { rut, nombre, apellido, fecha_nacimiento, domicilio, telefono, patentes, email } = req.body;
  rut = rut.toUpperCase();

  try {
      if (!verificarRut(rut)) {
          return res.status(400).json({ message: 'RUT inválido' });
      }

      // Verifica si ya existe una persona con el mismo RUT
      const existingPerson = await Person.findOne({ where: { rut } });

      if (existingPerson) {
          // La persona ya existe, actualiza los campos modificados
          await existingPerson.update({ rut, nombre, apellido, fecha_nacimiento, domicilio, telefono, email });
          console.log(`Persona con RUT ${rut} actualizada correctamente.`);
      } else {
          // La persona no existe, crea una nueva entrada
          await Person.create({ rut, nombre, apellido, fecha_nacimiento, domicilio, telefono, email });
          console.log(`Persona con RUT ${rut} insertada correctamente.`);
      }

      // Insertar las patentes en la tabla 'patents'
      for (let patent of patentes) {
          patent = patent.toUpperCase();
          if (verificarPatente(patent)) {
              const [newPatent, createdPatent] = await Patent.findOrCreate({
                  where: {
                      rut_person: rut,
                      patente: patent,
                  },
              });

              if (createdPatent) {
                  console.log(`Patente ${patent} asociada a la persona con RUT ${rut} insertada correctamente.`);
              } else {
                  console.log(`La patente ${patent} ya existe en la base de datos para la persona con RUT ${rut}. No se realizó ninguna inserción.`);
                  return res.status(402).json({ message: `Patente ${patent} ya existe en la base de datos.`})
              }
          } else {
              console.log(`Patente ${patent} no cumple con el formato válido.`);
              return res.status(403).json({ message: `Patente ${patent} no cumple con el formato válido.`})
          }
      }

      console.log('Proceso de inserción/actualización finalizado.');
      res.status(201).json({ message: 'Persona creada correctamente' });
  } catch (error) {
      console.error('Error al crear/actualizar personas:', error);
      res.status(500).json({ message: 'Error al crear/actualizar personas' });
  }
};
