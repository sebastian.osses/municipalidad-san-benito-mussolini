import { Inspector } from '../models/Inspector.js'; // Importa el modelo de Inspector
import { Infraction } from '../models/Infraction.js';
import { Op } from 'sequelize';


// get
export const getInspector = async (req, res) => {
  const { email, password } = req.query; // Obtén el email y la contraseña de la solicitud

  try {
    const inspector = await Inspector.findOne({
      where: {
        email: {
          [Op.eq]: email, // Busca por igualdad de email
        },
        password: {
          [Op.eq]: password, // Busca por igualdad de contraseña
        },
      },
    });

    if (inspector) {
      // Si se encuentra el inspector con las credenciales
      res.status(200).json(inspector);
    } else {
      // Si no se encuentra el inspector
      res.status(401).json({ message: 'Credenciales inválidas' });
    }
  } catch (error) {
    // Si ocurre un error en la consulta
    res.status(500).json({ message: 'Error al buscar el inspector' });
  }
};

//get infracciones del inspector
export const getInspectorInfractions = async (req, res) => {
  const { id } = req.params;
  console.log("id obtenido", id);

  try {
    // Buscar al inspector por ID
    const inspector = await Inspector.findOne({
      where: {
        id: {
          [Op.eq]: id,
        },
      },
    });

    if (!inspector) {
      // Si no se encuentra el inspector
      return res.status(404).json({ message: 'Inspector no encontrado' });
    }

    // Buscar las infracciones del inspector por su ID
    const infractions = await Infraction.findAll({
      where: {
        inspectorId: {
          [Op.eq]: id,
        },
      },
    });

    // Transforma el resultado para obtener el nombre del archivo
    const transformedInfractions = infractions.map(infraction => {
      const fileName = infraction.evidencia ? infraction.evidencia.toString().split('/').pop() : null;
      console.log(`el nombre del archivo es: ${fileName}`);
      return {
        ...infraction.dataValues,
        evidencia: fileName,
      };
    });

    // Devolver las infracciones con el nombre del archivo
    console.log(transformedInfractions);
    return res.status(200).json(transformedInfractions);
  } catch (error) {
    // Si ocurre un error en la consulta
    console.error('Error al obtener las infracciones del inspector:', error);
    return res.status(500).json({ message: 'Error al obtener las infracciones del inspector' });
  }
};

//post
export const createInspector = async (req, res) => {
    const { nombre, apellido, tipo,email, password } = req.body; // Obtén los datos del inspector desde el cuerpo de la solicitud
  
    try {
      // Verifica si ya existe un inspector con el mismo correo electrónico
      const existingInspector = await Inspector.findOne({
        where: {
          email: {
            [Op.eq]: email, // Busca por igualdad de email
          },
        },
      });
  
      if (existingInspector) {
        // Si ya existe un inspector con el mismo correo electrónico
        return res.status(400).json({ message: 'El correo electrónico ya está registrado' });
      }
  
      // Crea un nuevo inspector en la base de datos
      const newInspector = await Inspector.create({
        nombre,
        apellido,
        tipo,
        email,
        password,
      });
  
      // Envía una respuesta con el nuevo inspector creado
      console.log(newInspector);
      res.status(201).json({ message: 'Usuario creado correctamente.' });
    } catch (error) {
      // Si ocurre un error en la creación
      console.error('Error en la solicitud:', error);
      res.status(500).json({ message: 'Error al registrar el inspector' });
    }
  };
