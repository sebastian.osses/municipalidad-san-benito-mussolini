import express from "express";
import cors from 'cors';
import nodemailer from 'nodemailer';

import infractionsRoutes from './routes/infractions.routes.js'
import inspectorRoutes  from "./routes/inspector.routes.js";
import personRoutes from './routes/persons.routes.js';

const app = express();

// Configurar CORS para permitir solicitudes desde el frontend
app.use(cors({
  origin: 'http://localhost:3001',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
}));


// Resto de la configuración de tu aplicación Express
app.use(express.json()); // con esto puedo interpretar los json que me lleguen

app.use('/infractions', express.static('uploads'));
app.use('/inspector', express.static('uploads'));
app.use('/person', express.static('uploads'));

app.use(infractionsRoutes);
app.use(inspectorRoutes);
app.use(personRoutes);


// node mailer para envio de notificaciones

export const transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 465,
  secure: true,
  auth: {
    user: 'SENDER MAIL',
    pass: 'SMTP PASSCODE'
  }
});

transporter.verify().then(() => {
  console.log('Listo para enviar emails');
});


export default app;
