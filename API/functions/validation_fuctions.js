// funciones de condiciones:

export const verificarRut = (rut) => {
    var regex = /^[1-2]?[0-9]{6,7}-[0-9kK]$/;

    return regex.test(rut);
}

export const verificarPatente = (patente) => {
  // Expresión regular para verificar si la patente no contiene vocales
  const noVocalesRegex = /^[^aeiouAEIOU]+$/;
  // Expresión regular para verificar si la patente cumple con el patrón deseado
  const patenteRegex = /^(?:[A-Z]{2}\d{4}|[A-Z]{4}\d{2})$/;

  if (noVocalesRegex.test(patente) && patenteRegex.test(patente)) {
    return true; // La patente cumple con ambas condiciones
  } else {
    return false; // La patente no cumple con alguna de las condiciones
  }
}

export const formatDate = (date) => {
  const options = { year: 'numeric', month: 'long', day: 'numeric' };
  return new Date(date).toLocaleDateString(undefined, options);
};