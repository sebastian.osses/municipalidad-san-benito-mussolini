# Municipalidad San Benito Mussolini

# Grupo Abre Jaime

Este es el repositorio del *Grupo Abre Jaime*, cuyos integrantes son:

* José Acosta - 202004582-8
* Pedro Arce - 202056597-k
* Sebastian Osses - 202130525-4
* **Tutor**: Eduardo Pino

## Wiki

Puede acceder a la Wiki mediante el siguiente [enlace](https://gitlab.com/sebastian.osses/municipalidad-san-benito-mussolini/-/wikis/home)

## Videos

* [Video presentación Hito 1](https://youtu.be/K899qd1_GN8)

* [Video presentación Final](https://www.youtube.com/watch?v=dHPgVYYNAU8&ab_channel=JoseAcosta)

## Aspectos técnicos relevantes

Para el uso correcto del programa, se deben descargar las carpetas *'API'* y *'FRONT'* de este repositorio.
Además, se debe instalar:

* [Node.js](https://nodejs.org/en/download)
* [PostgreSQL](https://www.postgresql.org/download/)
* [Postman](https://www.postman.com/downloads/)
* [Docker](https://www.docker.com/)
* [Visual Studio Code](https://code.visualstudio.com/download)
* [Postman](https://www.postman.com/) o [ ClThunderient](https://www.thunderclient.com/) (extensión de VSC)

Teniendo todo lo anterior listo, procederemos a abrir SQL Shell de Postgres y crear nuestra base de datos:
```
CREATE DATABASE (nombre);
```

Luego, nos dirigiremos dentro de la carpeta *'API'* anteriormente descargada y modicaremos el archivo ".env" manuelmente de la siguiente manera:
```
DB_NAME=(nombre de la base de datos anteriormente creada)
DB_USER=(usuario de postgreSQL, por defecto: postgres)
DB_PASSWORD=(contraseña con la cual fue configurado PostgreSQL)
DB_HOST=localhost
DB_DIALECT=postgres
```

Después de esto, y siguiendo dentro de la carpeta *'API'*, abriremos una terminal cualquiera (Símbolo del sistema / cmd en el caso de Windows) y ejecutaremos:
```
npm install
```
```
npm start
```

En otra terminal, ejecutamos los mismo comandos pero dentro de la carpeta *'FRONT'*. 

De este modo estaremos ejecutando tanto nuestro BACKEND como nuestro FRONTEND, y se nos abrira una pestaña en el navegador para abrir el programa.


Para realizar solicitudes, adjuntamos una colección de POSTMAN (la cual se puede importar con el comando Ctrl + O) con las 2 consultas disponibles: *'GET'* y *'POST'*.


Donde se encuentra un ejemplo con la siguiente estructura:

![Estructura POST](./images/form_post.png)


Para la cual se debe obtener el siguiente tipo de respuesta:

![Estructura GET](./images/respuesta_post.png)

## Ejecución con Docker

Para ejecutar el proyecto usando docker, se debe editar el archivo ¨.env¨ de la carpeta API para conectarse a la base de datos. 

Además es necesario cambiar los datos del archivo docker-compose.yml en la parte de enviroment para que estos sean iguales a los de la base de datos

En la carpeta raíz del proyecto, donde se encuentre el docker-compose, ejecutar:
```
docker-compose up --build
```

Luego, abrir el contenedor desde Docker-Desktop, ya que se recomienda asignar memoria extra para la ejecución (3gb recomendado).
