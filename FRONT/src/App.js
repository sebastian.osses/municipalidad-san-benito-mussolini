import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';

import {Navbar} from './components/navbar';
import {Login} from './components/login/login';
import { Registration } from './components/login/register';

import { LoginPerson } from './components/loginPerson/loginPerson';
import { RegistrationPerson } from './components/loginPerson/registerPerson';

import {Home} from './components/home';


import {Person} from './components/person/person';

import {Infraction} from './components/infraction/infraction';
import {Inspector} from './components/inspector/inspector';

//modo dios
import { GodMode } from './components/godmode/godMode';
import { BenitoPay } from './components/benitopay/benitopay';


function App() {
  return (
    <Router>
      <Routes>
        <Route path="/benitopay" element={<BenitoPay />} />

        {/* Rutas que incluyen la barra de navegación */}
        <Route
          path="/*"
          element={
            <>
              <Navbar /> {/* Barra de navegación */}
              <Routes>
                <Route index element={<Home />} />
                <Route path="/login" element={<Login />} />
                <Route path="/register" element={<Registration />} />
                <Route path="/person/login" element={<LoginPerson />} />
                <Route path="/person/register" element={<RegistrationPerson />} />
                <Route path="/person" element={<Person />} />
                <Route path="/infractions" element={<Infraction />} />
                <Route path="/inspector" element={<Inspector />} />
                {/* Modo dios */}
                <Route path="/godmode" element={<GodMode />} />
              </Routes>
            </>
          }
        />
      </Routes>
    </Router>
  );
}

export default App;
