import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';

import Cookies from 'universal-cookie';

const cookies = new Cookies();

export const BenitoPay = () => {
  const id_infraccion = cookies.get('infractionId');
  console.log(id_infraccion)

  const [paymentData, setPaymentData] = useState({ bank: 'Banco del Señor', rut: '' });
  const [infraction, setInfraction] = useState({});

  useEffect(() => {

    axios.get(`http://localhost:3000/infractions/${id_infraccion}`)
      .then((response) => {
        setInfraction(response.data);
        console.log(infraction);
      })
      .catch((error) => {
        console.error('Error al obtener la infraccion:', error);
      });

  }, []);


  const handleInput = (e) => {
    const { name, value } = e.target;
    setPaymentData({ ...paymentData, [name]: value });
  };

  const handlePaymentSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.put(`http://localhost:3000/infractions/pay/${id_infraccion}`, {
        id: id_infraccion,
        rut: paymentData.rut,
        bank: paymentData.bank
      });

      // Muestra una alerta con el mensaje de éxito
      window.alert(`Éxito: ${response.data.message}`);
      
      if (response.status === 200) {
        cookies.remove('infractionId', { path: '/', sameSite: 'None', secure: true });
        window.location.href="/person";
      }
    } catch (error) {
      // Manejo de errores
      if (error.response) {
        // Si hay un error de respuesta del servidor, muestra solo el mensaje de error
        window.alert(`Error en la solicitud: ${error.response.data.message}`);
      } else {
        // Si hay un error no relacionado con la respuesta del servidor, muestra una alerta genérica
        window.alert('Ha ocurrido un error en la solicitud.');
      }
    }
  };

  return (
    <div className="d-flex justify-content-center align-items-center">
      <div className="p-3 bg-white w-25">
        {/* Encabezado del formulario de pagos */}
        <img src="/images/benitopay.png" alt="Portal de Pagos" className="mb-3" style={{ width: '100%' }} />

        {/* Monto a pagar */}
        <p className="mb-3">Monto a pagar: {infraction.monto}</p>

        {/* Formulario de pagos */}
        <form method="post" onSubmit={handlePaymentSubmit}>
          {/* Selector de bancos */}
          <label className="form-label">Selecciona tu banco</label>
          <select
            className="form-control"
            name="bank"
            value={paymentData.bank}
            onChange={handleInput}
            required
          >
            <option value="">Selecciona un banco</option>
            <option value="Banco del Señor">Banco del Señor</option>
            <option value="Benito Bank">Benito Bank</option>
            <option value="Banco SB">Banco SB</option>
          </select>

          <br />

          {/* Casilla de ingreso de RUT */}
          <label className="form-label">Ingresa tu RUT</label>
          <input
            type="text"
            className="form-control"
            name="rut"
            value={paymentData.rut}
            onChange={handleInput}
            placeholder="Formato: 12345678-9"
            required
          />

          <br />

          {/* Botón de PAGAR */}
          <button type="submit" className="btn benito-btn btn-primary ">
            Pagar
          </button>
        </form>
      </div>
    </div>
  );
};