import React, { useState } from 'react';
import axios from 'axios';

export const GodMode = () => {
  const [formValue, setFormValue] = useState({
    rut: '',
    nombre: '',
    apellido: '',
    fecha_nacimiento: '',
    domicilio: '',
    telefono: '',
    email: '',
    patentes: [''],
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormValue({ ...formValue, [name]: value });
  };

  const handlePatenteChange = (e, index) => {
    const newPatentes = [...formValue.patentes];
    newPatentes[index] = e.target.value;
    setFormValue({ ...formValue, patentes: newPatentes });
  };

  const addPatenteField = () => {
    setFormValue({ ...formValue, patentes: [...formValue.patentes, ''] });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post('http://localhost:3000/godmode', formValue);
      console.log(formValue);
      console.log(response.data);
      window.location.href = "./";
    } catch (error) {
      if (error.response) {
        // La solicitud se completó con una respuesta, pero pudo ser un error del servidor
        console.error('Error en la solicitud:', error.response.data);
      } else if (error.request) {
        // La solicitud se realizó, pero no se recibió respuesta
        console.error('No se recibió respuesta del servidor. Puede ser un problema de CORS.');
      } else {
        // Error al configurar o realizar la solicitud
        console.error('Error al configurar o realizar la solicitud:', error.message);
      }
    }
  };

  return (
    <div className="d-flex justify-content-center align-items-center">
      <div className="p-3 bg-white w-25">
        <h2>Creador de Personas</h2>
        <p> *** Este apartado en sí no es parte del programa, pero sirve para crear personas válidas dentro de la base de datos de ciudadanos. </p>
        <p>Acá se les puede asignar más de una patente a cada uno de estos, además que, el correo ingresado, será el correo por defecto al cual el ciudadano será notificado tras recibir una infracción. ***</p>
        
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="rut">RUT (sin puntos ni guión) </label>
            <input
              type="text"
              className="form-control"
              id="rut"
              name="rut"
              value={formValue.rut}
              onChange={handleInputChange}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="nombre">Nombre</label>
            <input
              type="text"
              className="form-control"
              id="nombre"
              name="nombre"
              value={formValue.nombre}
              onChange={handleInputChange}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="apellido">Apellido</label>
            <input
              type="text"
              className="form-control"
              id="apellido"
              name="apellido"
              value={formValue.apellido}
              onChange={handleInputChange}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="fecha_nacimiento">Fecha de Nacimiento</label>
            <input
              type="date"
              className="form-control"
              id="fecha_nacimiento"
              name="fecha_nacimiento"
              value={formValue.fecha_nacimiento}
              onChange={handleInputChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="domicilio">Domicilio</label>
            <input
              type="text"
              className="form-control"
              id="domicilio"
              name="domicilio"
              value={formValue.domicilio}
              onChange={handleInputChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="telefono">Teléfono</label>
            <input
              type="tel"
              className="form-control"
              id="telefono"
              name="telefono"
              value={formValue.telefono}
              onChange={handleInputChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              className="form-control"
              id="email"
              name="email"
              value={formValue.email}
              onChange={handleInputChange}
              required
              pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}"
              title="Ingrese una dirección de correo electrónico válida."
            />
          </div>
          <div className="form-group">
            <label htmlFor="patentes">Patentes</label>
            <p>*** El formato válido de las patentes es:</p>
            <p>4 letras (excluyendo vocales) seguidas de 2 numeros, por ej: XDDX23 </p> 
            <p>2 letras (excluyendo vocales) seguidas de 4 numeros, por ej: XD3419 ***</p>
            {formValue.patentes.map((patente, index) => (
              <div key={index}>
                <input
                  type="text"
                  className="form-control"
                  id={`patentes[${index}]`}
                  name={`patentes[${index}]`}
                  value={patente}
                  onChange={(e) => handlePatenteChange(e, index)}
                />
              </div>
            ))}
            <button type="button" onClick={addPatenteField} className="btn btn-secondary">
              Agregar Patente
            </button>
          </div>
          <button type="submit" className="btn btn-primary">Registrar Persona</button>
        </form>
      </div>
    </div>
  );
};