import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

const hasCookies = cookies.get('id') && cookies.get('nombre') && cookies.get('apellido');

const tipo = cookies.get('tipo');

export const Home = () => {
  const saludo = tipo === 'inspector' ? `Jesucristo te saluda, Inspector ${cookies.get('nombre')}` : `Jesucristo te saluda, ${cookies.get('nombre') || 'Invitado'}`;

  return (
    <div className="d-flex justify-content-center align-items-center">
      <div className="p-3 bg-white w-25">
        <div className="home">
          <h1>{saludo}</h1>
          <br/>
          <p>En un mundo lleno de incertidumbre y desafíos, la figura de Jesucristo brilla como un faro de esperanza y salvación. A lo largo de la historia, su mensaje de amor, perdón y redención ha tocado los corazones de millones de personas en todo el mundo, y su influencia perdura hasta el día de hoy.</p>

<p>Su amor incondicional por la humanidad se manifestó en su disposición a sacrificarse en la cruz para redimir nuestros pecados. A través de su sacrificio, nos mostró el verdadero significado del perdón y la misericordia divina. En su resurrección, nos dio la promesa de la vida eterna, un regalo que está al alcance de todos los que creen en Él.</p>

<p>Jesucristo también nos enseñó la importancia de amar a nuestros prójimos como a nosotros mismos, de cuidar a los necesitados y de ser compasivos. Sus parábolas y enseñanzas sobre la justicia, la humildad y la compasión siguen siendo una guía para vivir una vida significativa y plena.</p>

<p>A pesar de los desafíos y tribulaciones que enfrentamos en este mundo, la presencia de Jesucristo es una fuente constante de fortaleza y consuelo. Su mensaje de esperanza nos recuerda que no estamos solos en nuestras luchas y que, a través de la fe en Él, podemos encontrar la paz interior y la salvación eterna.</p>

<p>Además, en este camino de fe, no podemos olvidar la importancia del Pueblo Santo de San Benito, que recibe la gracia de Cristo y asegura la vida eterna para todos sus ciudadanos, al mismo tiempo que promueve la solidaridad y la reducción de impuestos para el bienestar de todos. Juntos, continuamos en la búsqueda de la verdad y la vida eterna.</p>
          {/* Agrega un botón para redirigir a la ruta /godMode */}
          <Link to="/godmode" className="btn btn-primary">MODO DIOS (para la creación de ciudadanos)</Link>
          </div>
        </div>
    </div>
  );
}