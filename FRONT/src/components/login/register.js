import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

export const Registration = () => {
  const [formValue, setFormValue] = useState({
    nombre: '',
    apellido: '',
    tipo: 'inspector',
    email: '',
    password: '',
  });

  const handleInput = (e) => {
    const { name, value } = e.target;
    setFormValue({ ...formValue, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    
    try {
        const response = await axios.post(
            'http://localhost:3000/inspector', // Cambia la URL a la ruta de registro de tu API
            formValue
        );
        console.log(formValue);
        console.log(response.data);

        // Muestra una alerta con la respuesta específica del servidor
        window.alert(`Respuesta del servidor: ${JSON.stringify(response.data.message)}`);

        window.location.href = "./login";
      } catch (error) {
        if (error.response) {
          // Si hay un error de respuesta del servidor, muestra solo la respuesta específica
          const errorMessage = error.response.data.message;
          window.alert(`Error en la solicitud: ${errorMessage}`);
        } else {
          // Si hay un error no relacionado con la respuesta del servidor, muestra una alerta genérica
          window.alert('Ha ocurrido un error en la solicitud.');
        }
      }
    };


  return (
    <div className="d-flex justify-content-center align-items-center">
      <div className="p-3 bg-white w-25">
        <h2>Registro</h2>
        <form method="post" onSubmit={handleSubmit}>
          <label className="form-label">Nombre</label>
          <input
            type="text"
            className="form-control"
            name="nombre"
            value={formValue.nombre}
            onChange={handleInput}
            placeholder="Ingrese su nombre"
            required
          />

          <br />
          <label className="form-label">Apellido</label>
          <input
            type="text"
            className="form-control"
            name="apellido"
            value={formValue.apellido}
            onChange={handleInput}
            placeholder="Ingrese su apellido"
            required
          />

          <br />
          <label className="form-label">Email</label>
          <input
            type="email"
            className="form-control"
            name="email"
            value={formValue.email}
            onChange={handleInput}
            placeholder="Ingrese su correo electrónico"
            required
          />
          <br />
          <label className='form-label'>Tipo</label>
          <select className='form-control' name="tipo" value={formValue.tipo} onChange={handleInput}>
            <option value="inspector">Inspector Municipal</option>
            <option value="operador">Operador Municipal</option>
          </select>

          <br />

          <br />
          <label className="form-label">Contraseña</label>
          <input
            type="password"
            className="form-control"
            name="password"
            value={formValue.password}
            onChange={handleInput}
            placeholder="Ingrese su contraseña"
            required
          />

          <br />

          <div className="d-flex justify-content-between align-items-center">
            <button type="submit" className="btn">
              Registrarse
            </button>
            <Link to="/login">Login</Link>
          </div>
        </form>
      </div>
    </div>
  );
};
