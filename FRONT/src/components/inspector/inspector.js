import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

export const Inspector = () => {
  const id_inspector = cookies.get('id');
  const nombre = cookies.get('nombre');
  const apellido = cookies.get('apellido');
  const email = cookies.get('email');

  const [infractions, setInfractions] = useState([]);
  const [showEvidence, setShowEvidence] = useState(false);
  const [evidenceUrl, setEvidenceUrl] = useState('');

  useEffect(() => {
    // Agregar el id_inspector como un parámetro en la URL
    axios.get(`http://localhost:3000/inspector/${id_inspector}/infractions`)
      .then((response) => {
        // Cuando se reciba la respuesta exitosa, establecer el estado de infracciones
        setInfractions(response.data);
      })
      .catch((error) => {
        console.error('Error al obtener las infracciones:', error);
      });
  }, []);

  const formatDate = (date) => {
    // Formatear la fecha como día, mes y año
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(date).toLocaleDateString(undefined, options);
  };

  const handleShowEvidence = (url) => {
    setEvidenceUrl(url);
    setShowEvidence(true);
  };

  return (
    <div className="d-flex justify-content-center align-items-center">
      <div className="p-3 bg-white w-75">
        <h2>DATOS DEL INSPECTOR</h2>
        <div>
          <p>ID: {id_inspector}</p>
          <p>Nombre: {nombre}</p>
          <p>Apellido: {apellido}</p>
          <p>Email: {email}</p>
        </div>
        <h2>INFRACCIONES IMPARTIDAS</h2>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Patente</th>
              <th>RUT</th>
              <th>Tipo</th>
              <th>Criticidad</th>
              <th>Fecha Emisión</th>
              <th>Fecha Expiración</th>
              <th>Estado</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {infractions.map((infraction) => (
              <tr key={infraction.id}>
                <td>{infraction.id}</td>
                <td>{infraction.patente}</td>
                <td>{infraction.rut}</td>
                <td>{infraction.tipo}</td>
                <td>{infraction.criticidad}</td>
                <td>{formatDate(infraction.fecha_emision)}</td>
                <td>{formatDate(infraction.fecha_vencimiento)}</td>
                <td>{infraction.estado}</td>
                <td>
                  <button
                    onClick={() => handleShowEvidence(`http://localhost:3000/uploads/${infraction.evidencia}`)}
                    className="btn btn-primary"
                  >
                    Mostrar Evidencia
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      {showEvidence && (
        <div className="modal" tabIndex="-1" role="dialog" style={{ display: 'block' }}>
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Evidencia</h5>
                <button
                  type="button"
                  className="close"
                  onClick={() => setShowEvidence(false)}
                >
                  <span>&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <img src={evidenceUrl} alt="Evidencia" style={{ width: '100%' }} />
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};