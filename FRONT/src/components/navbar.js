import React from 'react';
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

export const Navbar = () => {
  const handleLogout = () => {
    // Elimina las cookies específicas que deseas borrar
    cookies.remove('id', { path: '/', sameSite: 'None', secure: true });
    cookies.remove('nombre', { path: '/', sameSite: 'None', secure: true });
    cookies.remove('apellido', { path: '/', sameSite: 'None', secure: true });
    cookies.remove('email', { path: '/', sameSite: 'None', secure: true });
    cookies.remove('tipo', { path: '/', sameSite: 'None', secure: true });
    cookies.remove('infractionId', { path: '/', sameSite: 'None', secure: true });
    // Redirige al usuario a la página de inicio de sesión
    window.location.href = '/'; // Cambia la URL según sea necesario
  };

  // Obtén el valor de 'tipo' de las cookies
  const tipo = cookies.get('tipo');

  // Verifica la presencia de las cookies
  const hasCookies = cookies.get('id') && cookies.get('nombre') && cookies.get('apellido');

  return (
    <nav className='navbar'>
      <div className='nav-left a'>

        {/* Enlace a la página de inicio */}
        <Link to="/">Inicio</Link>

        {/* Enlace a la página de infracciones */}
        {hasCookies && tipo === 'inspector' && <Link to="/infractions">Infracción</Link>}
        {hasCookies && tipo === 'inspector' && <Link to="/inspector">Inspector</Link>}

        {/* Muestra "Mis Infracciones" si es un 'person' o si no está logueado */}
        {(hasCookies && tipo === 'person') && <Link to="/person">Mis Infracciones</Link>}

        {/* Cerrar Sesión */}
        {hasCookies ? (
          <Link to="/" onClick={handleLogout} className="logout-link">
            Cerrar Sesión
          </Link>
        ) : (
          <>
            <Link to="/person/login" className="login-person-link">Acceso Ciudadano</Link>
            <Link to="/login" className="login-link">Acceso Municipal</Link>
          </>
        )}
      </div>

      <div className='nav-right a'>
        <a>Municipalidad San Benito</a>
      </div>
    </nav>
  );
};