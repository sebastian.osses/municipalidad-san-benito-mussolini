import React, {useState} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';

import Cookies from 'universal-cookie';

const cookies = new Cookies();


export const Infraction = () => {

    const [formValue, setFormValue] = useState({patente:'',tipo:'',criticidad:'baja',detalles:'',monto:'', evidencia: null})
    

    const handleInput = (e) => {
      const {name, value}= e.target;
      setFormValue({...formValue, [name]:value});
    };

    const handleFileChange = (e) => {
      const file = e.target.files[0];
      setFormValue({ ...formValue, evidencia: file });
    };
    
    const handleSubmit = async (e) => {
      e.preventDefault();

      const id_inspector = cookies.get('id');

      const formData = new FormData();
      formData.append('inspectorId', id_inspector);
      //formData.append('rut', formValue.rut);
      formData.append('patente', formValue.patente);
      formData.append('tipo', formValue.tipo);
      formData.append('criticidad', formValue.criticidad);
      formData.append('detalles', formValue.detalles);
      formData.append('monto', formValue.monto);
      formData.append('evidencia', formValue.evidencia);
    
      console.log(formData);
    
      try {
        const response = await axios.post('http://localhost:3000/infractions', formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        });
    
        // Muestra una alerta con el mensaje de éxito
        window.alert(`Éxito: ${response.data.message}`);
      } catch (error) {
        if (error.response) {
          // Si hay un error de respuesta del servidor, muestra solo el mensaje de error
          window.alert(`Error en la solicitud: ${error.response.data.message}`);
        } else {
          // Si hay un error no relacionado con la respuesta del servidor, muestra una alerta genérica
          window.alert('Ha ocurrido un error en la solicitud.');
        }
      }
    };

  return (
    <div className="d-flex  justify-content-center align-items-center">
      <div className="p-3 bg-white w-25">

      <form method="post" onSubmit={handleSubmit} encType="multipart/form-data">
        {/*  
        <label className='form-label'>RUT del conductor</label>
        <input type="text" className='form-control' name="rut" value={formValue.rut} onChange={handleInput} placeholder="formato: 12345678-9" />
        
        <br />
        */}
        <label className='form-label'>Patente</label>
        <input type="text" className='form-control' name="patente" value={formValue.patente} onChange={handleInput} placeholder="formato: XXXX00 o XX0011" />
        
        <br />
        <label className='form-label'>Tipo de Infraccion</label>
        <input type="text" className='form-control' name="tipo" value={formValue.tipo} onChange={handleInput}  placeholder="Ingrese Tipo" />
       

        <br />
        <label className='form-label'>Criticidad</label>
        <select className='form-control' name="criticidad" value={formValue.criticidad} onChange={handleInput}>
          <option value="baja">Baja</option>
          <option value="media">Media</option>
          <option value="alta">Alta</option>
        </select>

        <br />
        <label className='form-label'>Detalles</label>
        <textarea className='form-control' id="detalles" name="detalles" value={formValue.detalles} onChange={handleInput} placeholder="Ingrese Detalles"></textarea>
      
        <br />
        <label className='form-label'>Monto</label>
        <input type="text" className='form-control' name="monto" value={formValue.monto} onChange={handleInput} placeholder="Ingrese Monto" />

        <br />

        <label className='form-label'>Evidencia</label>
        <input type="file" className='form-control' name="evidencia" onChange={handleFileChange} />

        <br />

        <button type="submit" className='btn'>Submit</button>
      </form>
      </div>
    </div>
  );
}