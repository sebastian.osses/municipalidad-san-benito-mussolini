import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import Cookies from 'universal-cookie';
import axios from 'axios';

const cookies = new Cookies();

export const LoginPerson = () => {
    // Estado para almacenar los valores del formulario
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
  
    // Función para manejar el envío del formulario
    const handleSubmit = async (e) => {
        e.preventDefault();
        
        //console.log(`Email: ${email}, Password: ${password}`);
        await axios.get("http://localhost:3000/person", {params: {email: email, password: password}})
        .then(response=>{
            if (response.status === 200 && response.data) {
                var person = response.data;
                cookies.set('id', person.id, { path: "/" , sameSite: 'None', secure: true });
                cookies.set('nombre', person.nombre, { path: "/" , sameSite: 'None', secure: true });
                cookies.set('apellido', person.apellido, { path: "/" , sameSite: 'None', secure: true });
                cookies.set('email', email, { path: "/" , sameSite: 'None', secure: true });
                cookies.set('tipo', 'person', { path: "/" , sameSite: 'None', secure: true });
                console.log(person);
                window.location.href="/";
            } else {
                window.alert('El usuario o la contraseña es inválido')
            }
        })
        .catch(error=>{
            console.log(error);
            window.alert('El usuario o la contraseña es inválido')
        })
        
    };
  
    return (
    <div className="d-flex justify-content-center align-items-center">
        <div className="p-3 bg-white w-25">
            <h2>Iniciar Sesión</h2>
            <form onSubmit={handleSubmit}>
            <div className="form-group">
                <label>Email</label>

                <br />

                <input
                type="email" className='form-control'
                placeholder="Ingrese su correo electrónico"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
                />
            </div>

            <br />

            <div className="form-group">
                <label>Contraseña</label>

                <br />

                <input
                type="password" className='form-control'
                placeholder="Ingrese su contraseña"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
                />
            </div>

            <br />
            <div className="d-flex justify-content-between align-items-center">
                <button type="submit" className='btn'>Iniciar Sesion</button>
                <Link to="/person/register">Registro</Link>
            </div>
            </form>
            
        </div>
        </div>
    );
  }
  
