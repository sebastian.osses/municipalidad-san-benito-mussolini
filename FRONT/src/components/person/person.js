import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

export const Person = () => {
  const id_person = cookies.get('id');
  const email = cookies.get('email');

  const [person, setPerson] = useState({});
  const [infractions, setInfractions] = useState([]);
  const [showEvidence, setShowEvidence] = useState(false);
  const [evidenceUrl, setEvidenceUrl] = useState('');

  useEffect(() => {
    // Realiza una solicitud para obtener los datos de la persona por su ID
    axios.get(`http://localhost:3000/person/${id_person}`)
      .then((response) => {
        // Cuando se reciba la respuesta exitosa, establecer el estado de la persona
        setPerson(response.data);
      })
      .catch((error) => {
        console.error('Error al obtener los datos de la persona:', error);
      });

    // Luego, realiza una solicitud para obtener las infracciones de la persona
    axios.get(`http://localhost:3000/person/${id_person}/infractions`)
      .then((response) => {
        // Cuando se reciba la respuesta exitosa, establecer el estado de las infracciones
        setInfractions(response.data);
      })
      .catch((error) => {
        console.error('Error al obtener las infracciones:', error);
      });
  }, []);

  const formatDate = (date) => {
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(date).toLocaleDateString(undefined, options);
  };

  const handleShowEvidence = (url) => {
    setEvidenceUrl(url);
    setShowEvidence(true);
  };

  const handlePay = (infractionId) => {

    cookies.set('infractionId', infractionId, { path: "/" , sameSite: 'None', secure: true });

    window.location.href="/benitopay";
  };

  const handleAppeal = async (infractionId) => {
    try {
      // Realizar solicitud PUT a la ruta de apelación
      const response = await axios.put(`http://localhost:3000/infractions/appeal/${infractionId}`);
  
      // Capturar el mensaje de respuesta del servidor
      const message = response.data.message;
  
      // Actualizar el estado local de las infracciones después de apelar
      setInfractions((prevInfractions) =>
        prevInfractions.map((infraction) =>
          infraction.id === infractionId ? { ...infraction, estado: 'en apelacion' } : infraction
        )
      );
  
      // Mostrar el mensaje de respuesta como una alerta
      alert(message);
    } catch (error) {
      console.error('Error al apelar la infracción:', error);
      alert('Error al apelar la infracción');
    }
  };

  return (
    <div className="d-flex justify-content-center align-items-center">
      <div className="p-3 bg-white w-75">
        <h2>DATOS DEL CIUDADANO</h2>
        <div>
          <p>ID: {person.id}</p>
          <p>RUT: {person.rut}</p>
          <p>Nombre: {person.nombre}</p>
          <p>Apellido: {person.apellido}</p>
          <p>Fecha de Nacimiento: {formatDate(person.fecha_nacimiento)}</p>
          <p>Domicilio: {person.domicilio}</p>
          <p>Teléfono: {person.telefono}</p>
          <p>Patentes: {person.patentes ? person.patentes.map(patente => patente).join(', ') : 'N/A'}</p>
          <p>Email: {email}</p>
        </div>
        <h2>INFRACCIONES RECIBIDAS</h2>
        <table className="table">
          <thead>
          <tr>
              <th>ID</th>
              <th>Patente</th>
              <th>RUT</th>
              <th>Tipo</th>
              <th>Criticidad</th>
              <th>Fecha Emisión</th>
              <th>Fecha Expiración</th>
              <th>Monto CLP$</th>
              <th>Estado</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
          {infractions.map((infraction) => (
            <tr key={infraction.id}>
              <td>{infraction.id}</td>
              <td>{infraction.patente}</td>
              <td>{infraction.rut}</td>
              <td>{infraction.tipo}</td>
              <td>{infraction.criticidad}</td>
              <td>{formatDate(infraction.fecha_emision)}</td>
              <td>{formatDate(infraction.fecha_vencimiento)}</td>
              <td>{infraction.monto}</td>
              <td>{infraction.estado}</td>
              <td>
                {(infraction.estado === 'activa' || infraction.estado === 'vencida' )&& (
                  <>
                    <button
                      onClick={() => handleShowEvidence(`http://localhost:3000/uploads/${infraction.evidencia}`)}
                      className="btn btn-primary"
                    >
                      Mostrar Evidencia
                    </button>
                    <button className="btn btn-success" style={{ marginLeft: '10px' }} onClick={() => handlePay(infraction.id)}>Pagar</button>
                    <button className="btn btn-danger" style={{ marginLeft: '10px' }} onClick={() => handleAppeal(infraction.id)}>Apelar</button>
                  </>
                )}

                {infraction.estado === 'pagada' && (
                  <p>Todo en Orden</p>
                )}

                {infraction.estado === 'desestimada' && (
                  <p>Nada que realizar</p>
                )}

                {infraction.estado === 'en apelacion' && (
                  <>
                    <button
                      onClick={() => handleShowEvidence(`http://localhost:3000/uploads/${infraction.evidencia}`)}
                      className="btn btn-primary"
                    >
                      Mostrar Evidencia
                    </button>
                    <button className="btn btn-success" style={{ marginLeft: '10px' }} disabled>Pagar</button>
                    <button className="btn btn-danger" style={{ marginLeft: '10px' }} disabled>Apelar</button>
                  </>
                )}
              </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      {showEvidence && (
        <div className="modal" tabIndex="-1" role="dialog" style={{ display: 'block' }}>
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Evidencia</h5>
                <button
                  type="button"
                  className="close"
                  onClick={() => setShowEvidence(false)}
                >
                  <span>&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <img src={evidenceUrl} alt="Evidencia" style={{ width: '100%' }} />
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};